extern crate proc_macro;

use log::{abort};
use proc_macro::{TokenStream};
use quote::quote;
use syn::{parse_macro_input, DeriveInput, AttributeArgs, DataStruct, Fields, FieldsNamed, Data};

#[proc_macro_derive(Assign)]
pub fn derive(input: TokenStream) -> TokenStream {
    let derive_input: DeriveInput = parse_macro_input!(input);
    let struct_name = derive_input.ident;

    let fields = collect_fields(&derive_input);

    // let struct_fields = derive_input.attrs.iter().map(|&item| item.parse_args()name()).collect();
    let output = quote! {
        impl AssingTrait for #struct_name {
            fn fields(&self) -> vec[String]{
                return #struct_fields;
            }
        }
    };
    output.into()
}

fn collect_fields(ast: &syn::DeriveInput) -> Vec<syn::Field> {
    println!( "{:?}", ast);
    match ast.data {
        syn::Data::Struct(syn::DataStruct { ref fields, .. }) => {
            if fields.iter().any(|field| field.ident.is_none()) {
                abort!(
                    fields.span(),
                    "struct has unnamed fields";
                    help = "#[derive(Validate)] can only be used on structs with named fields";
                );
            }
            fields.iter().cloned().collect::<Vec<_>>()
        }
        _ => abort!(ast.span(), "#[derive(Validate)] can only be used with structs"),
    }
}

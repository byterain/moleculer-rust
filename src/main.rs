mod broker;
use broker::broker::{Broker, BrokerOptions};

mod services;
use services::service::ServiceSchema;

const MATH2SERVICE: ServiceSchema = ServiceSchema{
    name: "math".to_string(),
    version: "1".to_string(),
    dependencies: vec![],
    settings: Default::default(),
    actions: vec![],
    methods: vec![],
    created: None,
    started: None,
    stopped: None
};

fn main() {
    let mathService = ServiceSchema{
        name: "".to_string(),
        version: "".to_string(),
        dependencies: vec![],
        settings: Default::default(),
        actions: vec![],
        methods: vec![],
        created: None,
        started: None,
        stopped: None
    };
    let mut broker = Broker::new(None);
    broker.set_name_space("test".to_string());
    broker.create_service("ds".to_string(), mathService);
    broker.start();
    println!("Hello, world!");
}

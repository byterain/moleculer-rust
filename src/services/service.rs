use std::any::Any;
use std::collections::HashMap;
use serde_json::Value;
use serde_json::json;
use crate::broker::broker::{Broker};

#[path = "../context/context.rs"]
mod context;
use context::Context;

type ActionHandler = fn(&Service, &Context);
type LifecycleHandler = fn(&Service);

pub struct Action {
    name: *mut str,
    version: i32,
    handler: ActionHandler,
}

trait ServiceTrait {
    fn get_name(&self) -> &str;
    fn get_version(&self) -> &str;
    fn get_metadata(&self) -> &Value;
}

#[derive(Clone, Debug)]
pub struct ServiceSchema {
    pub(crate) name: String,
    pub(crate) version: String,
    pub(crate) dependencies: Vec<String>,
    pub(crate) settings: Value,
    pub(crate) actions: Vec<Action>,
    pub(crate) methods: Vec<Action>,
    pub(crate) created: Option<fn(&Service, &Context)>,
    pub(crate) started: Option<fn(&Service, &Context)>,
    pub(crate) stopped: Option<fn(&Service, &Context)>,
    // metadata    : map[string]interface{},
    // hooks       : map[string]interface{},
    // mixins      : []Mixin,
    // actions     : []Action,
    // events      : []Event,
    // created     : LifecycleHandler,
    // started     : LifecycleHandler,
    // stopped     : LifecycleHandler
}


pub struct Mixin {
    //     Name         string
//     Dependencies []string
//     Settings     map[string]interface{}
// Metadata     map[string]interface{}
// Hooks        map[string]interface{}
// Actions      []Action
// Events       []Event
// Created      CreatedFunc
// Started      LifecycleFunc
// Stopped      LifecycleFunc
    name: String,
    version: i32,
    dependencies: Vec<String>,
    actions: Vec<Action>,
    methods: Vec<Action>,
    // Hooks        map[string]interface{}
    // Events       []Event
    created: fn(&Service, &Context),
    started: fn(&Service, &Context),
    stopped: fn(&Service, &Context),
}


fn test() {
    vec!(1, 2);
}

pub struct Service<'a> {
    pub(crate) name: String,
    version: String,
    schema: ServiceSchema,
    broker: &'a Broker<'a>,
    settings: Value,
    metadata: Value,
    dependencies: Vec<String>,
    action: Vec<Action>,
    methods: Vec<Action>,
    // Hooks        mapVec<string]interface{}
    // Events       Vec<]Event
    created: Vec<LifecycleHandler>,
    started: Vec<LifecycleHandler>,
    stopped: Vec<LifecycleHandler>,
}

impl<'a> ServiceTrait for Service<'a> {
    fn get_name(&self) -> &str {
        return self.name.as_str();
    }

    fn get_version(&self) -> &str {
        return self.version.as_str();
    }

    fn get_metadata(&self) -> &Value {
        return &self.metadata;
    }
}

impl<'a> Service<'a> {
    pub(crate) fn new(schema: ServiceSchema, broker: &'a mut Broker) -> Service<'a> {
        Self {
            name: "".to_string(),
            version: "".to_string(),
            broker,
            schema,
            settings: Default::default(),
            metadata: Default::default(),
            dependencies: vec![],
            action: vec![],
            methods: vec![],
            created: vec![],
            started: vec![],
            stopped: vec![],
        }
    }
    fn call() {

    }
}

macro_rules! service_schema {
    {
        structName: $struct_name:ident,
        name: $service_name:literal,
        dependencies: [$($dependencies:expr),+ $(,)?] ,
        settings: $($settings:tt)+
    } => {

        pub struct $struct_name {
            name: String,
            version: i32,
            settings: Value,
            metadata: Value,
            dependencies: Vec<String>,
            action: Vec<Action>,
            methods: Vec<Action>,
            // Hooks        mapVec<string]interface{}
            // Events       Vec<]Event
            created: Vec<LifecycleHandler>,
            started: Vec<LifecycleHandler>,
            stopped: Vec<LifecycleHandler>,
        }

        impl $struct_name {
            fn new() -> Self{
                Self {
                    name: "ss".to_string(),
                    version: 0,
                    dependencies: Vec::from([$($dependencies.to_string()),+]),
                    settings: json!($($settings)+),
                    metadata: json!({}),
                    action: vec![],
                    methods: vec![],
                    created: vec![],
                    started: vec![],
                    stopped: vec![],

                }
            }
        }
    }
}

service_schema! {
    structName: TestService,
    name: "test.service",
    dependencies: ["test"],
    settings: {
        "port": 3000,
    }
}



use std::collections::HashMap;
use tokio::runtime::*;
use crate::services::service::{Service, ServiceSchema};

enum ServiceResolver<'a>{
    Service(&Service),
    ServiceSchema(&ServiceSchema)
}

pub struct RetryPolicy {
    enabled: bool,
    retries: i8,
    delay: i32,
    max_delay: i32,
    factor: i32,
    check: String,
}

pub struct Tracking {
    enabled: bool,
    shutdown_timeout: i32,
}

pub struct Registry {
    strategy: String,
    prefer_local: bool,
}

pub struct Bulkhead {
    enabled: bool,
    concurrency: i32,
    max_queue_size: i32,
}



pub struct Transit {
    max_queue_size: i32,
    // 50k ~ 400MB,
    max_chunk_size: i32,
    // 256KB
    disable_reconnect: bool,
    disable_version_check: bool,
}

pub struct CircuitBreak {
    enabled: bool,
    threshold: f32,
    window_time: i8,
    min_request_count: i8,
    half_open_time: i32,
    check: String,
}

pub struct BrokerOptions {
    pub namespace: String,
    pub node_id: String,

    pub logger: bool,
    pub log_level: String,

    pub transporter: String, //"TCP",

    pub error_regenerator: String,

    pub request_timeout: i32,
    pub retry_policy: RetryPolicy,

    pub context_params_cloning: bool,
    pub max_call_level: i32,
    pub heartbeat_interval: i32,
    pub heartbeat_timeout: i32,
    pub tracking: Tracking,
    pub disable_balancer: bool,
    pub registry: Registry,
    pub circuit_breaker: CircuitBreak,
    pub bulkhead: Bulkhead,
    pub transit: Transit,
    pub uid_generator: String,

    pub error_handler: String,

    pub cacher: String,
    pub serializer: String,

    pub validator: bool,
    pub metrics: bool,
    pub tracing: bool,
    pub internal_services: bool,
    pub internal_middlewares: bool,
    pub dependency_interval: i32,
    pub hot_reload: bool,
    pub middlewares: String,

    pub repl_commands: bool,
    pub repl_delimiter: bool,

    pub metadata: String,
    pub skip_process_event_registration: bool,
    pub max_safe_object_size: bool,
// ServiceFactory: null,
// ContextFactory: null
// Promise: null
}

pub struct Broker<'a> {
    pub options: BrokerOptions,
    services: HashMap<String, &'a Service<'a>>
}

impl<'a> Broker<'a> {
    pub fn new(options: Option<BrokerOptions>) -> Self {
        let options = options.unwrap_or(BrokerOptions {
            namespace: "".to_string(),
            node_id: "".to_string(),
            logger: true,
            log_level: "".to_string(),
            transporter: "".to_string(),
            error_regenerator: "".to_string(),
            request_timeout: 0,
            retry_policy: RetryPolicy {
                enabled: false,
                retries: 0,
                delay: 0,
                max_delay: 0,
                factor: 0,
                check: "".to_string(),
            },
            context_params_cloning: false,
            max_call_level: 0,
            heartbeat_interval: 10,
            heartbeat_timeout: 30,
            tracking: Tracking { enabled: false, shutdown_timeout: 5000 },
            disable_balancer: false,
            registry: Registry { strategy: "RoundRobin".to_string(), prefer_local: true },
            circuit_breaker: CircuitBreak {
                enabled: false,
                threshold: 0.5,
                window_time: 60,
                min_request_count: 20,
                half_open_time: 10 * 1000,
                check: "".to_string(),
            },
            bulkhead: Bulkhead {
                enabled: false,
                concurrency: 10,
                max_queue_size: 100,
            },
            transit: Transit {
                max_queue_size: 50 * 1000,
                max_chunk_size: 256 * 1024,
                disable_reconnect: false,
                disable_version_check: false,
            },
            uid_generator: "".to_string(),
            error_handler: "".to_string(),
            cacher: "".to_string(),
            serializer: "".to_string(),
            validator: false,
            metrics: false,
            tracing: false,
            internal_services: false,
            internal_middlewares: false,
            dependency_interval: 0,
            hot_reload: false,
            middlewares: "".to_string(),
            repl_commands: false,
            repl_delimiter: false,
            metadata: "".to_string(),
            skip_process_event_registration: false,
            max_safe_object_size: false,
        });
        Self {
            options,
            services: HashMap::new()
        }
    }

    pub fn set_name_space(&mut self, namespace: String) -> &Self {
        self.options.namespace = namespace;
        self
    }

    pub fn test(&self) {
        println!("teste");
    }
    pub fn emit(&self) {
        println!("teste");
    }
    pub fn create_service(&mut self, name: String, service_resolver: ServiceResolver) -> &'a Service {
        return match service_resolver {
            ServiceResolver::Service(service) => {
                self.services.insert(service.name.to_string(), &service);
                &service
            }
            ServiceResolver::ServiceSchema(service_schema) => {
                let service = Service::new(service_schema.clone(), &mut self);
                self.services.insert(service.name, &service);
                &service
            }
        }
    }

    pub fn start(&mut self) {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            println!("starting runtime");
            loop {}
        });
    }
}
